﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6.MainComponent.Contract;
using Lab6.Display.Contract;

namespace Lab6.MainComponent.Implementation
{
    public class MainComponent : IMainComponent
    {
        public void metoda1()
        {
        Console.WriteLine("metoda 1");
        }
        public void metoda2()
        {
            Console.WriteLine("metoda 2");
        }
        private IDisplay display;
        public MainComponent(IDisplay display)
        {
            this.display = display;
        }


    }
}
