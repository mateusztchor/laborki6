﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Container.NET;
using PK.Container;
using System.Reflection;

namespace Lab6.Container
{
    public class Kontener : IContainer
    {
        private IDictionary<Type, Type> typy = new Dictionary<Type, Type>();
        private IDictionary<Type, Object> objekty = new Dictionary<Type, Object>();

        public void Register<T>(Func<T> provider) where T : class
        {
            Register(provider.Invoke() as T);
        }

        public void Register<T>(T impl) where T : class
        {
            var inter = impl.GetType().GetInterfaces();
            int dl = inter.Length;
            for (int i = 0; i < dl; i++)
            {
                if (!typy.ContainsKey(inter[i]))
                {
                    objekty.Add(inter[i], impl);
                }
                else objekty[inter[i]] = impl;
            }

        }

        public void Register(Type type)
        {
            Type[] wezTypy = type.GetInterfaces();
            foreach (Type item in wezTypy)
            {
                if (typy.ContainsKey(item))
                {
                    typy[item] = type;
                }
                else typy.Add(item, type);
            }
        }

        public void Register(System.Reflection.Assembly assembly)
        {
            Type[] wezTypy = assembly.GetTypes();
            foreach (var item in wezTypy)
            {
                if (item.IsPublic)
                {
                    this.Register(item);
                }
            }

        }

        public object Resolve(Type type)
        {
            return null;
        }

        public T Resolve<T>() where T : class
        {
            return (T)Resolve(typeof(T));
        }



    }
}