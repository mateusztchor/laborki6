﻿using Lab6.ControlPanel.Contract;
using PK.Container;
using System;
using System.Reflection;
using Lab6.Display.Contract;
using Lab6.Display.Implementation;
using Lab6.MainComponent.Contract;
using Lab6.MainComponent.Implementation;
using Lab6.ControlPanel.Implementation;
using Lab6.Container;


namespace Lab6.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Func<IContainer> ContainerFactory = () => new Kontener();

        public static Assembly ControlPanelSpec = Assembly.GetAssembly(typeof(IControlPanel));
        public static Assembly ControlPanelImpl = Assembly.GetAssembly(typeof(ControlPanel.Implementation.ControlPanel));

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(IMainComponent));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(void));

        #endregion
    }
}
