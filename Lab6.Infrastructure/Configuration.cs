﻿using PK.Container;
using System;
using System.Reflection;
using Lab6.Container;
using Lab6.ControlPanel.Contract;
using Lab6.ControlPanel.Implementation;
using Lab6.Display.Contract;
using Lab6.Display.Implementation;
using Lab6.MainComponent.Contract;
using Lab6.MainComponent.Implementation;

namespace Lab6.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            var container = LabDescriptor.ContainerFactory();
            container.Register(new Lab6.Display.Implementation.Display());
            container.Register(LabDescriptor.ControlPanelImpl);
            container.Register(LabDescriptor.MainComponentImpl);

            return container;
        }
    }
}
