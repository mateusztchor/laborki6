﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6.ControlPanel.Contract;
using Lab6.MainComponent.Contract;


namespace Lab6.ControlPanel.Implementation
{
    public class ControlPanel :IControlPanel
    {
        public System.Windows.Window Window
        {
            get { return p; }
        }
        private Panel p = new Panel();

        private IMainComponent m;
        public ControlPanel(IMainComponent m)
        {
            this.m = m;
        }

    }
}
